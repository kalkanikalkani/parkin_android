package com.chris.parkin.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.UserDetailModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.UriHelper;
import com.chris.parkin.util.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserAccountActivity extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 102;
    private static final int SELECT_FILE = 2;

    ImageView idIvLastnameEdit, idIvFirstnameEdit, idIvLastnameOK, idIvFirstnameOK;
    EditText idEdtLastName, idEdtFirstname;
    FirebaseFirestore db;
    ImageView idIvuserImage;
    CardView layoutImageEdit;
    private String TAG = UserAccountActivity.class.getSimpleName();
    private Uri selectedImageUri;
    private File fileUri;
    private File selectedProfile;
    FirebaseStorage firebaseStorage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_account);
        db = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
        setupToolbar();
        idIvFirstnameEdit = findViewById(R.id.idIvFirstnameEdit);
        idIvLastnameEdit = findViewById(R.id.idIvLastnameEdit);
        idIvFirstnameOK = findViewById(R.id.idIvFirstnameOK);
        idIvLastnameOK = findViewById(R.id.idIvLastnameOK);
        idEdtFirstname = findViewById(R.id.idEdtFirstname);
        idEdtLastName = findViewById(R.id.idEdtLastName);
        idIvuserImage = findViewById(R.id.idIvuserImage);
        layoutImageEdit = findViewById(R.id.layoutImageEdit);

        idIvLastnameEdit.setOnClickListener(this);
        idIvFirstnameEdit.setOnClickListener(this);
        idIvLastnameOK.setOnClickListener(this);
        idIvFirstnameOK.setOnClickListener(this);
        layoutImageEdit.setOnClickListener(this);

        idEdtFirstname.setText(Utils.getUserDetail(this).getName());
        idEdtLastName.setText(Utils.getUserDetail(this).getLname());
        if (Utils.getUserDetail(this).getPhotoPath() != null && !Utils.getUserDetail(this).getPhotoPath().isEmpty()) {
            Picasso.get().load(Utils.getUserDetail(this).getPhotoPath()).placeholder(R.drawable.default_img).into(idIvuserImage);
        }
    }

    public void setupToolbar() {
        ImageView idIvBack = findViewById(R.id.idIvBack);
        TextView idTvTitle = findViewById(R.id.idTvTitle);
        TextView idTvSubTitle = findViewById(R.id.idTvSubTitle);
        idTvTitle.setText(R.string.user_profile_text);
        idTvSubTitle.setVisibility(View.GONE);
        idIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.idIvFirstnameEdit:
                if (!checkValidation(idEdtLastName)) {
                    idEdtFirstname.setEnabled(true);
                    idEdtLastName.setEnabled(false);
                    idIvFirstnameEdit.setVisibility(View.GONE);
                    idIvFirstnameOK.setVisibility(View.VISIBLE);
                    idIvLastnameEdit.setVisibility(View.VISIBLE);
                    idIvLastnameOK.setVisibility(View.GONE);
                }
                break;
            case R.id.idIvLastnameEdit:
                if (!checkValidation(idEdtFirstname)) {
                    idEdtLastName.setEnabled(true);
                    idEdtFirstname.setEnabled(false);
                    idIvFirstnameEdit.setVisibility(View.VISIBLE);
                    idIvFirstnameOK.setVisibility(View.GONE);
                    idIvLastnameEdit.setVisibility(View.GONE);
                    idIvLastnameOK.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.idIvFirstnameOK:
                showProgressDialog(this, "", getString(R.string.loading_text));
                setDataToFirebase(idEdtFirstname, Constant.FS_USERS_USERNAME);
                break;
            case R.id.idIvLastnameOK:
                showProgressDialog(this, "", getString(R.string.loading_text));
                setDataToFirebase(idEdtLastName, Constant.FS_USERS_LASTNAME);
                break;
            case R.id.layoutImageEdit:
                selectImage();
                break;
        }
    }

    public boolean checkValidation(EditText editText) {
        boolean b = false;
        if (editText.getText().toString().isEmpty()) {
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            editText.startAnimation(shake);
            b = true;
        }
        return b;
    }

    public void setDataToFirebase(final EditText editText, final String fieldName) {
        Map<String, Object> deviceData = new HashMap<>();
        deviceData.put(fieldName, editText.getText().toString());
        db.collection(Constant.FS_USERS).document(Utils.getUserDetail(this).getUid()).set(deviceData, SetOptions.merge()).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dismissProgressDialog();
                UserDetailModel userDetailModel = Utils.getUserDetail(UserAccountActivity.this);
                if (fieldName == Constant.FS_USERS_USERNAME) {
                    userDetailModel.setName(editText.getText().toString());
                } else {
                    userDetailModel.setLname(editText.getText().toString());
                }
                Utils.setUserDetail(UserAccountActivity.this, userDetailModel);
            }
        }).

                addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dismissProgressDialog();
                        Toast.makeText(UserAccountActivity.this, R.string.api_fail_text, Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(UserAccountActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                // boolean result=Utility.checkPermission(MainActivity.this);
                if (items[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = Utils.getOutputMediaFile();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Logg.e(TAG, "SDK >= N 24");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", fileUri));
                    } else {
                        Logg.e(TAG, "SDK: " + Build.VERSION.SDK_INT);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileUri));
                    }
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Choose from Library")) {
                    if (!Utils.checkPermissionStorage(UserAccountActivity.this)) {
                        ActivityCompat.requestPermissions(UserAccountActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_STORAGE);
                    } else {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);//
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {


        if (data != null) {

            selectedImageUri = data.getData();
            beginCrop(selectedImageUri);
        }
    }

    private void onCaptureImageResult(Intent data) {
        beginCrop(Uri.fromFile(fileUri));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);

        }
        if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }

    }

    private void beginCrop(Uri source) {
        Calendar calendar = Calendar.getInstance();
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped" + calendar.getTimeInMillis() + ".png"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            idIvuserImage.setImageURI(Crop.getOutput(result));
            String path = UriHelper.getPath(UserAccountActivity.this, Crop.getOutput(result));
            selectedProfile = new File(path);
            Toast.makeText(this, "" + selectedProfile, Toast.LENGTH_SHORT).show();
            uploadImage();


        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadImage() {

        if (selectedProfile != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child("user_images/" + UUID.randomUUID().toString());
            ref.putFile(Uri.fromFile(selectedProfile))
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Logg.e("**uploaded uri", taskSnapshot.getDownloadUrl() + "");
                            Map<String, Object> userData = new HashMap<>();
                            userData.put(Constant.FS_USERS_IMAGE, taskSnapshot.getDownloadUrl().toString());
                            db.collection(Constant.FS_USERS).document(Utils.getUserDetail(UserAccountActivity.this).getUid()).set(userData, SetOptions.merge());
                            UserDetailModel userDetailModel = Utils.getUserDetail(UserAccountActivity.this);
                            userDetailModel.setPhotoPath(taskSnapshot.getDownloadUrl().toString());
                            Utils.setUserDetail(UserAccountActivity.this, userDetailModel);
                            Toast.makeText(UserAccountActivity.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(UserAccountActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);//
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);

                } else {

                    Toast.makeText(this, getResources().getString(R.string.storage_access_text), Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

}
