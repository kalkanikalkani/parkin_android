package com.chris.parkin.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.AddressModel;
import com.chris.parkin.model.BookingModel;
import com.chris.parkin.model.DeviceModel;
import com.chris.parkin.model.PaymentModel;
import com.chris.parkin.model.PaypalPaymentModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Utils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalAuthorization;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class BookingDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = TestActivity.class.getSimpleName();

    String client_id = "Ac44GPXOS6MuC1P7WoXOjI9EFFWycmf2whMwbiYDS882rV_TFgS5Fe8gtmf7TeCKFHdKW4X7fhlBUGpn";
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    //private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    private PayPalPayment thingToBuy;
    AddressModel addressModel;
    String totalPrice;
    Intent intent;
    Button idBtnPay;
    TextView idTvTitle, idTvFromDate, idTvToDate, idTvAmount, idTvAddress, idTvCity, idTvParkingSpaceDetail;
    ImageView idIvImage, idIvBack;
    String fromDate, toDate;
    FirebaseFirestore db;
    private DeviceModel deviceModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);
        intent = getIntent();
        addressModel = new Gson().fromJson(intent.getStringExtra("data"), AddressModel.class);
        totalPrice = intent.getStringExtra("totalPrice");
        fromDate = intent.getStringExtra("fromDate");
        toDate = intent.getStringExtra("toDate");
        deviceModel = new Gson().fromJson(intent.getStringExtra("deviceData"), DeviceModel.class);
        idBtnPay = findViewById(R.id.idBtnPay);
        idTvTitle = findViewById(R.id.idTvTitle);
        idTvFromDate = findViewById(R.id.idTvFromDate);
        idTvToDate = findViewById(R.id.idTvToDate);
        idTvAmount = findViewById(R.id.idTvAmount);
        idTvAddress = findViewById(R.id.idTvAddress);
        idTvCity = findViewById(R.id.idTvCity);
        idIvImage = findViewById(R.id.idIvImage);
        idIvBack = findViewById(R.id.idIvBack);
        idTvParkingSpaceDetail = findViewById(R.id.idTvParkingSpaceDetail);

        idBtnPay.setOnClickListener(this);
        idIvBack.setOnClickListener(this);

        idTvTitle.setText(addressModel.getTitle());
        idTvAddress.setText(addressModel.getAddress());
        idTvAmount.setText("$" + totalPrice);
        idTvCity.setText(addressModel.getCity() + "," + addressModel.getState() + "," + addressModel.getCountry() + "," + addressModel.getPin_code());
        if (addressModel.getImage() != null && !addressModel.getImage().isEmpty()) {
            Picasso.get().load(addressModel.getImage()).into(idIvImage);
        }
        idTvFromDate.setText(Utils.getFormattedDateString("yyyy-MM-dd HH:mm", "dd MMM, yyyy hh:mm a", fromDate));
        idTvToDate.setText(Utils.getFormattedDateString("yyyy-MM-dd HH:mm", "dd MMM, yyyy hh:mm a", toDate));
        idTvParkingSpaceDetail.setText(deviceModel.getName());

        paypalConfiguration();

        db = FirebaseFirestore.getInstance();
    }

    public void paypalConfiguration() {
        PayPalConfiguration config = new PayPalConfiguration()
                .environment(CONFIG_ENVIRONMENT)
                .clientId(client_id)
                // the following are only used in PayPalFuturePaymentActivity.
                .merchantName("Hipster Store")
                .merchantPrivacyPolicyUri(
                        Uri.parse("https://www.example.com/privacy"))
                .merchantUserAgreementUri(
                        Uri.parse("https://www.example.com/legal"));


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);
    }


    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(BookingDetailActivity.this,
                PayPalFuturePaymentActivity.class);

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject()
                                .toString(4));

                        final PaypalPaymentModel paypalPaymentModel = new Gson().fromJson(confirm.toJSONObject().toString(4), PaypalPaymentModel.class);


                        //fill booking detail,device detail and payment detail
                        BookingModel bookingModel = new BookingModel();
                        bookingModel.setStatus(true);
                        bookingModel.setAddress_id(addressModel.getAddress_id());
                        bookingModel.setAmount(totalPrice);
                        bookingModel.setDevice_id(deviceModel.getDevice_id());
                        bookingModel.setEnd_time(Utils.getTimeStampFromString("yyyy-MM-dd HH:mm", toDate));
                        bookingModel.setStart_time(Utils.getTimeStampFromString("yyyy-MM-dd HH:mm", fromDate));
                        bookingModel.setUser_id(Utils.getUserDetail(this).getUid());
                        final DocumentReference ref = db.collection(Constant.FS_BOOKING).document();
                        bookingModel.setBooking_id(ref.getId());

                        ref.set(bookingModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Map<String, Object> deviceData = new HashMap<>();
                                deviceData.put(Constant.FS_DEVICES_IS_BOOKING, true);
                                db.collection(Constant.FS_DEVICES).document(deviceModel.getDevice_id())
                                        .set(deviceData, SetOptions.merge());

                                PaymentModel paymentModel = new PaymentModel();
                                paymentModel.setAmount(totalPrice);
                                paymentModel.setBooking_id(ref.getId());
                                paymentModel.setCreated_time(new Date());
                                paymentModel.setPlatform("Android");
                                paymentModel.setPayment_id(paypalPaymentModel.getResponse().getId());
                                paymentModel.setStatus(paypalPaymentModel.getResponse().getState());
                                paymentModel.setUser_id(Utils.getUserDetail(BookingDetailActivity.this).getUid());
                                db.collection(Constant.FS_PAYMENT).document().set(paymentModel).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {

                                        Toast.makeText(BookingDetailActivity.this, "payment successfully done with adding records also", Toast.LENGTH_SHORT).show();

                                        Intent intent=new Intent(BookingDetailActivity.this,BookingCompletionActivity.class);
                                        intent.putExtra("fromDate",fromDate);
                                        startActivity(intent);
                                    }
                                });

                            }
                        });


                        Toast.makeText(getApplicationContext(), "Order placed",
                                Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        Toast.makeText(getApplicationContext(),
                                "Future Payment code received from PayPal",
                                Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.");
            }
        }
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {

    }

    public void onFuturePaymentPurchasePressed(View pressed) {
        // Get the Application Correlation ID from the SDK
        String correlationId = PayPalConfiguration
                .getApplicationCorrelationId(this);

        Log.i("FuturePaymentExample", "Application Correlation ID: "
                + correlationId);

        // TODO: Send correlationId and transaction details to your server for
        // processing with
        // PayPal...
        Toast.makeText(getApplicationContext(),
                "App Correlation ID received from SDK", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.idBtnPay:
                thingToBuy = new PayPalPayment(new BigDecimal(totalPrice), "USD",
                        "Pay for parking slot", PayPalPayment.PAYMENT_INTENT_SALE);
                Intent intent = new Intent(BookingDetailActivity.this,
                        PaymentActivity.class);

                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
                break;

            case R.id.idIvBack:
                finish();
                break;
        }
    }
}
