package com.chris.parkin.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.UserDetailModel;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
     //   boolean b = checkDateBeforeOrAfter();
     //   Toast.makeText(this, "b: " + b, Toast.LENGTH_SHORT).show();

         changeActivity();

    }

    public void changeActivity() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                UserDetailModel userData = Utils.getUserDetail(SplashActivity.this);
                if (userData != null) {
                    if (userData.getUid() != "") {
                        Intent i = new Intent(SplashActivity.this, DashboardActivity.class);
                        startActivity(i);
                    } else {
                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


    boolean checkDateBeforeOrAfter() //String startDate,String endDate,String myStartDate,String myEndDate
    {
        boolean b = true;
        String startDate = "03/26/2018 07:00:00";
        String endDate = "03/26/2018 09:00:00";
        String myStartDate = "03/26/2018 09:00:00";
        String myEndDate = "03/26/2018 10:00:00";

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy hh:mm:ss");
        Date min, max , mysdate, myedate;
        try {
            min = dateFormat.parse(startDate);
            max = dateFormat.parse(endDate);
            mysdate = dateFormat.parse(myStartDate);
            myedate = dateFormat.parse(myEndDate);

            if(startDate.equals(myStartDate) || endDate.equals(myedate))
            {
                b=false;
            }
            if ((mysdate.after(min) && mysdate.before(max))) {
                Logg.e("**date1", "mysdate.after(min)" + mysdate.after(min));
                Logg.e("**date2", "mysdate.before(max)" + mysdate.before(max));
                b = false;
            }
            if (myedate.after(min) && myedate.before(max)) {
                Logg.e("**date3", "myedate.after(min)" + myedate.after(min));
                Logg.e("**date4", "myedate.before(max)" + myedate.before(max));
                b = false;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }
}
