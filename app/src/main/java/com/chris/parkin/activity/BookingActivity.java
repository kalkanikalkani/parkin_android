package com.chris.parkin.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.AddressModel;
import com.chris.parkin.model.BookingModel;
import com.chris.parkin.model.DeviceModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BookingActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = BookingActivity.class.getSimpleName();
    EditText idEdtFromDate, idEdtFromTime, idEdtToDate, idEdtToTime;
    private Calendar newFromCalendar, newToCalendar;
    TextView idTvTitle, idTvSubTitle;
    SimpleDateFormat dateFormatter;
    Intent intent;
    AddressModel addressModel = new AddressModel();
    ImageView idIvBack;
    private Button idBtnGoAhead;
    FirebaseFirestore db;
    private List<DeviceModel> deviceModels;
    private List<BookingModel> bookingModels;
    private int device_number = -1;
    private List<BookingModel> bookedArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        intent = getIntent();
        db = FirebaseFirestore.getInstance();
        deviceModels = new ArrayList<>();
        bookingModels = new ArrayList<>();
        bookedArray = new ArrayList<>();
        idEdtFromDate = findViewById(R.id.idEdtFromDate);
        idEdtFromTime = findViewById(R.id.idEdtFromTime);
        idEdtToDate = findViewById(R.id.idEdtToDate);
        idEdtToTime = findViewById(R.id.idEdtToTime);
        idTvTitle = findViewById(R.id.idTvTitle);
        idTvSubTitle = findViewById(R.id.idTvSubTitle);
        idBtnGoAhead = findViewById(R.id.idBtnGoAhead);
        idIvBack = findViewById(R.id.idIvBack);

        idEdtFromDate.setOnClickListener(this);
        idEdtFromTime.setOnClickListener(this);
        idEdtToDate.setOnClickListener(this);
        idEdtToTime.setOnClickListener(this);
        idBtnGoAhead.setOnClickListener(this);
        idIvBack.setOnClickListener(this);

        addressModel = new Gson().fromJson(intent.getStringExtra("data"), AddressModel.class);
        newFromCalendar = Calendar.getInstance();
        newToCalendar = Calendar.getInstance();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        idTvTitle.setText(addressModel.getAddress());
        idTvSubTitle.setText("$" + addressModel.getPrice() + " " + getString(R.string.per_hour_text));
        idEdtFromDate.setText(dateFormatter.format(newFromCalendar.getTime()));
        idEdtFromTime.setText(Utils.twoDigitString(newFromCalendar.get(Calendar.HOUR_OF_DAY)) + ":" + Utils.twoDigitString(newFromCalendar.get(Calendar.MINUTE)));

        newToCalendar.add(Calendar.HOUR_OF_DAY, 1);
        idEdtToDate.setText(dateFormatter.format(newToCalendar.getTime()));
        idEdtToTime.setText(Utils.twoDigitString(newToCalendar.get(Calendar.HOUR_OF_DAY)) + ":" + Utils.twoDigitString(newToCalendar.get(Calendar.MINUTE)));

        getAllDeviceOFAddress();
        setTotalAmountToBePaid();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.idEdtFromDate:
                showFromDateCalendar();
                break;
            case R.id.idEdtFromTime:
                showFromTimeCalendar();
                break;
            case R.id.idEdtToDate:
                showToDateCalendar();
                break;
            case R.id.idEdtToTime:
                showToTimeCalendar();
                break;
            case R.id.idBtnGoAhead:
                getAllBookingOfAddress();


                break;
            case R.id.idIvBack:
                finish();
                break;
        }
    }

    public void getAllDeviceOFAddress() {
        db.collection(Constant.FS_DEVICES).whereEqualTo(Constant.FS_DEVICES_ADDRESS_ID, addressModel.getAddress_id()).whereEqualTo(Constant.FS_DEVICES_STATUS, true).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.getResult().size() != 0) {
                    deviceModels = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        Logg.e(TAG, "devices : " + document.getId() + " => " + document.getData());
                        Map<String, Object> hashMap = document.getData();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(hashMap);
                        DeviceModel deviceModel = gson.fromJson(jsonElement, DeviceModel.class);
                        deviceModel.setDevice_id(document.getId());
                        deviceModels.add(deviceModel);
                    }
                } else {
                    Toast.makeText(BookingActivity.this, R.string.no_device_found_on_adddress, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void getAllBookingOfAddress() {
        db.collection(Constant.FS_BOOKING).whereEqualTo(Constant.FS_BOOKING_ADDRESS_ID, addressModel.getAddress_id()).whereEqualTo(Constant.FS_BOOKING_STATUS, true).whereGreaterThanOrEqualTo(Constant.FS_BOOKING_START_TIME, Utils.getTimeStampFromString("yyyy-MM-dd HH:mm", idEdtFromDate.getText().toString() + " " + idEdtFromTime.getText().toString())).whereLessThanOrEqualTo(Constant.FS_BOOKING_START_TIME, Utils.getTimeStampFromCurrentDayEndDateTime(idEdtFromDate.getText().toString())).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                bookingModels = new ArrayList<>();

                if (task.getResult().size() != 0) {
                    for (DocumentSnapshot document : task.getResult()) {
                        Logg.e(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> hashMap = document.getData();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(hashMap);
                        BookingModel bookingModel = gson.fromJson(jsonElement, BookingModel.class);
                        bookingModel.setBooking_id(document.getId());
                        bookingModels.add(bookingModel);
                    }
                }
                if (bookingModels.size() != 0) {

                    for (int j = 0; j < bookingModels.size(); j++) {
                        Logg.e("**dates", Utils.getDateStringFromTimeStamp(bookingModels.get(j).getStart_time(), "yyyy-MM-dd HH:mm") + " " + Utils.getDateStringFromTimeStamp(bookingModels.get(j).getEnd_time(), "yyyy-MM-dd HH:mm"));

                        if (checkDateBeforeOrAfter(Utils.getDateStringFromTimeStamp(bookingModels.get(j).getEnd_time(), "yyyy-MM-dd HH:mm"), Utils.getDateStringFromTimeStamp(bookingModels.get(j).getStart_time(), "yyyy-MM-dd HH:mm"), idEdtFromDate.getText().toString() + " " + idEdtFromTime.getText().toString(), idEdtToDate.getText().toString() + " " + idEdtToTime.getText().toString())) {
                            Toast.makeText(BookingActivity.this, "" + true + " " + bookingModels.get(j).getBooking_id(), Toast.LENGTH_SHORT).show();
                        } else {
                            bookedArray.add(bookingModels.get(j));
                        }
                    }
                }

                if (bookedArray.size() != 0) {
                    for (int i = 0; i < deviceModels.size(); i++) {
                        for (int j = 0; j < bookedArray.size(); j++) {
                            if (deviceModels.get(i).getDevice_id().equals(bookedArray.get(j).getDevice_id())) {
                                deviceModels.remove(i);
                            }
                        }
                    }
                }
                try {
                    Random random = new Random();
                    device_number = random.nextInt(deviceModels.size());


                    if (device_number != -1) {
                        Toast.makeText(BookingActivity.this, "" + deviceModels.get(device_number).getName(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(BookingActivity.this, BookingDetailActivity.class);
                        intent.putExtra("data", new Gson().toJson(addressModel));
                        intent.putExtra("deviceData", new Gson().toJson(deviceModels.get(device_number)));
                        intent.putExtra("totalPrice", setTotalAmountToBePaid());
                        intent.putExtra("fromDate", idEdtFromDate.getText().toString() + " " + idEdtFromTime.getText().toString());
                        intent.putExtra("toDate", idEdtToDate.getText().toString() + " " + idEdtToTime.getText().toString());
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(BookingActivity.this, getString(R.string.no_device_found_on_adddress), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    boolean checkDateBeforeOrAfter(String startDate, String endDate, String myStartDate, String myEndDate) {
        boolean b = true;
        //String myStartDate = "03/26/2018 09:00:00";
        // String myEndDate = "03/26/2018 10:00:00";

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm");
        Date min, max, mysdate, myedate;
        try {
            min = dateFormat.parse(startDate);
            max = dateFormat.parse(endDate);
            mysdate = dateFormat.parse(myStartDate);
            myedate = dateFormat.parse(myEndDate);

            if (startDate.equals(myStartDate) || endDate.equals(myedate)) {
                b = false;
            }
            if ((mysdate.after(min) && mysdate.before(max))) {
                Logg.e("**date1", "mysdate.after(min)" + mysdate.after(min));
                Logg.e("**date2", "mysdate.before(max)" + mysdate.before(max));
                b = false;
            }
            if (myedate.after(min) && myedate.before(max)) {
                Logg.e("**date3", "myedate.after(min)" + myedate.after(min));
                Logg.e("**date4", "myedate.before(max)" + myedate.before(max));
                b = false;
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }


    public void showFromDateCalendar() {
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                idEdtFromDate.setText(dateFormatter.format(newDate.getTime()));
                setTotalAmountToBePaid();
            }

        }, newFromCalendar.get(Calendar.YEAR), newFromCalendar.get(Calendar.MONTH), newFromCalendar.get(Calendar.DAY_OF_MONTH));

        datePicker.getDatePicker().setMinDate(newFromCalendar.getTime().getTime());
        datePicker.setTitle("");
        datePicker.show();
    }

    public void showFromTimeCalendar() {
        final int hour, minute, currentHour, currentMinute;
        if (!idEdtFromTime.getText().toString().isEmpty()) {
            hour = Integer.parseInt(idEdtFromTime.getText().toString().split(":")[0]);
            minute = Integer.parseInt(idEdtFromTime.getText().toString().split(":")[1]);
        } else {
            hour = newFromCalendar.get(Calendar.HOUR_OF_DAY);
            minute = newFromCalendar.get(Calendar.MINUTE);
        }
        currentHour = newFromCalendar.get(Calendar.HOUR_OF_DAY);
        currentMinute = newFromCalendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(BookingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String[] toHour = idEdtToTime.getText().toString().split(":");
                if (selectedHour == currentHour) {
                    if (selectedMinute <= currentMinute) {
                        Toast.makeText(BookingActivity.this, "Choose valid time", Toast.LENGTH_SHORT).show();
                    } else {
                        idEdtFromTime.setText(Utils.twoDigitString(selectedHour) + ":" + Utils.twoDigitString(selectedMinute));
                        if (selectedHour >= Integer.parseInt(toHour[0])) {
                            idEdtToTime.setText(Utils.twoDigitString(selectedHour + 1) + ":" + Utils.twoDigitString(selectedMinute));
                        }
                    }
                } else if (selectedHour > currentHour) {

                    idEdtFromTime.setText(Utils.twoDigitString(selectedHour) + ":" + Utils.twoDigitString(selectedMinute));
                    if (selectedHour >= Integer.parseInt(toHour[0])) {
                        idEdtToTime.setText(Utils.twoDigitString(selectedHour + 1) + ":" + Utils.twoDigitString(selectedMinute));
                    }

                } else {
                    Toast.makeText(BookingActivity.this, "Choose valid time", Toast.LENGTH_SHORT).show();
                }
                setTotalAmountToBePaid();

            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setTitle("");
        mTimePicker.show();
    }

    public String setTotalAmountToBePaid() {
        String s = idEdtFromDate.getText().toString() + " " + idEdtFromTime.getText().toString();
        String ss = idEdtToDate.getText().toString() + " " + idEdtToTime.getText().toString();

        Date d = Utils.getFormattedDate("yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm", s);
        Date dd = Utils.getFormattedDate("yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm", ss);

        double val = (Utils.getMinutesFromDate(dd, d) * Double.parseDouble(String.valueOf(addressModel.getPrice()))) / Double.parseDouble(String.valueOf(60));
        idBtnGoAhead.setText(getString(R.string.proveed_to_pay_text) + " $" + String.format("%.2f", val) + "");

        Logg.e("**ddfddd", d + " " + dd);
        return String.format("%.2f", val);
    }

    //public void changeScreen()


    public void showToDateCalendar() {
        DatePickerDialog datePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                idEdtToDate.setText(dateFormatter.format(newDate.getTime()));
                setTotalAmountToBePaid();
            }

        }, newToCalendar.get(Calendar.YEAR), newToCalendar.get(Calendar.MONTH), newToCalendar.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMinDate(newToCalendar.getTime().getTime());
        datePicker.setTitle("");
        datePicker.show();
    }

    public void showToTimeCalendar() {
        final int hour, minute;
        if (!idEdtToTime.getText().toString().isEmpty()) {
            hour = Integer.parseInt(idEdtToTime.getText().toString().split(":")[0]);
            minute = Integer.parseInt(idEdtToTime.getText().toString().split(":")[1]);
        } else {
            hour = newToCalendar.get(Calendar.HOUR_OF_DAY);
            minute = newToCalendar.get(Calendar.MINUTE);
        }
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(BookingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                String[] fromHour = idEdtFromTime.getText().toString().split(":");
                if (idEdtFromDate.getText().toString().equals(idEdtToDate.getText().toString())) {
                    if (selectedHour == Integer.parseInt(fromHour[0])) {
                        Toast.makeText(BookingActivity.this, "Minimum 1 hour", Toast.LENGTH_SHORT).show();
                    } else if (selectedHour < Integer.parseInt(fromHour[0])) {
                        Toast.makeText(BookingActivity.this, "Choose valid time", Toast.LENGTH_SHORT).show();
                    } else {
                        idEdtToTime.setText(Utils.twoDigitString(selectedHour) + ":" + Utils.twoDigitString(selectedMinute));
                    }
                } else {
                    idEdtToTime.setText(Utils.twoDigitString(selectedHour) + ":" + Utils.twoDigitString(selectedMinute));
                }

                setTotalAmountToBePaid();
            }
        }, hour, minute, true);//Yes 24 hour time

        mTimePicker.setTitle("");
        mTimePicker.show();
    }

}

