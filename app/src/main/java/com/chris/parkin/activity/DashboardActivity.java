package com.chris.parkin.activity;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.UserDetailModel;
import com.chris.parkin.model.AddressModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.firebase.functions.HttpsCallableResult;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult>, MapboxMap.OnMarkerClickListener, View.OnClickListener {

    private static final int REQUEST_CHECK_SETTINGS = 101;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private static final String TAG = DashboardActivity.class.getSimpleName();
    private TextView idIvUserName, idTvDirection, idTvBook, idIvUserEmail, idTvAway, idTvParkingAddress, idTvAvailableSpace, idTvTotalSpace, idTvRate;
    private ImageView idIvUserProfile, idIvParkingImage;
    private FirebaseFirestore db;
    private MapView mapView;
    private GoogleApiClient mGoogleApiClient;
    private LatLng mLatLng;
    private MarkerOptions mMarker;
    private MapboxMap mMapBoxMap;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationRequest mLocationRequest;
    private boolean mRequestingLocationUpdates = false;
    private CarmenFeature home, work;
    private BottomSheetBehavior sheetBehavior;
    LinearLayout layoutBottomSheet;
    private View overView;
    private FloatingActionButton searchFab, idFabMyLocation;
    List<AddressModel> addressModels = new ArrayList<>();
    private Marker UserMarker;
    private ArrayList<Marker> markersOfAddresses = new ArrayList<>();
    private DrawerLayout drawer;
    private int selectedPosition = 0;
    private FirebaseFunctions functions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.map_token));
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_dashboard);

        db = FirebaseFirestore.getInstance();
        functions = FirebaseFunctions.getInstance();

        // TODO: 13-03-2018 map integrate

        mapView = (MapView) findViewById(R.id.idMvDashboardMapView);
        mapView.onCreate(savedInstanceState);
        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        overView = findViewById(R.id.overView);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        idIvParkingImage = findViewById(R.id.idIvParkingImage);
        idTvAvailableSpace = findViewById(R.id.idTvAvailableSpace);
        idTvParkingAddress = findViewById(R.id.idTvParkingAddress);
        idTvTotalSpace = findViewById(R.id.idTvTotalSpace);
        idTvRate = findViewById(R.id.idTvRate);
        idTvAway = findViewById(R.id.idTvAway);
        idTvDirection = findViewById(R.id.idTvDirection);
        idTvBook = findViewById(R.id.idTvBook);
        searchFab = (FloatingActionButton) findViewById(R.id.searchFab);
        idFabMyLocation = findViewById(R.id.idFabMyLocation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.transparent));
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        searchFab.setOnClickListener(this);
        idTvDirection.setOnClickListener(this);
        idTvBook.setOnClickListener(this);
        idFabMyLocation.setOnClickListener(this);
        overView.setOnClickListener(this);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mMapBoxMap = mapboxMap;

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        //Location Permission already granted
                        buildGoogleApiClient();
                    } else {
                        //Request Location Permission
                        checkLocationPermission();
                    }
                } else {
                    buildGoogleApiClient();
                }
            }
        });
        addUserLocations();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        initView(headerView);

        db.collection(Constant.FS_ADDRESS)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        Gson gson = new Gson();
                        if (e != null) {
                            Log.w(TAG, "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    Log.d(TAG, "New city: " + dc.getDocument().getData());
                                    Map<String, Object> hashMap = dc.getDocument().getData();

                                    JsonElement jsonElement = gson.toJsonTree(hashMap);
                                    AddressModel addressModel = gson.fromJson(jsonElement, AddressModel.class);
                                    addressModel.setAddress_id(dc.getDocument().getId());
                                    addressModels.add(addressModel);
                                    addMarkerOFDevices(addressModel, dc.getDocument().getId());
                                    break;
                                case MODIFIED:
                                    Log.d(TAG, "Modified city: " + dc.getDocument().getData());
                                    Map<String, Object> hashMapModified = dc.getDocument().getData();

                                    JsonElement jsonElementModified = gson.toJsonTree(hashMapModified);
                                    AddressModel addressModelMdified = gson.fromJson(jsonElementModified, AddressModel.class);
                                    addressModelMdified.setAddress_id(dc.getDocument().getId());
                                    Logg.e("**address modified", addressModelMdified.getLat() + " " + addressModelMdified.getLng());
                                    for (int i = 0; i < addressModels.size(); i++) {
                                        if (addressModels.get(i).getAddress_id().equals(dc.getDocument().getId())) {

                                            addressModels.set(i, addressModelMdified);
                                        }
                                    }
                                    for (int i = 0; i < markersOfAddresses.size(); i++) {
                                        if (markersOfAddresses.get(i).getTitle().equals(dc.getDocument().getId())) {
                                            markersOfAddresses.get(i).setPosition(new LatLng(Double.parseDouble(addressModelMdified.getLat()), Double.parseDouble(addressModelMdified.getLng())));
                                            Logg.e("**marker address", markersOfAddresses.get(i).getPosition().getLatitude() + " " + markersOfAddresses.get(i).getPosition().getLongitude());
                                        }
                                    }
                                    break;
                                case REMOVED:
                                    Map<String, Object> hashMapRemoved = dc.getDocument().getData();

                                    JsonElement jsonElementRemoved = gson.toJsonTree(hashMapRemoved);
                                    AddressModel addressModelRemoved = gson.fromJson(jsonElementRemoved, AddressModel.class);
                                    addressModelRemoved.setAddress_id(dc.getDocument().getId());
                                    Log.d(TAG, "Removed city: " + dc.getDocument().getData());

                                    for (int i = 0; i < markersOfAddresses.size(); i++) {
                                        if (markersOfAddresses.get(i).getTitle().equals(dc.getDocument().getId())) {
                                            Marker marker = markersOfAddresses.get(i);
                                            marker.remove();
                                            addressModels.remove(i);
                                        }
                                    }
                                    break;
                            }
                        }

                    }
                });
    }

    private void addUserLocations() {
        home = CarmenFeature.builder().text("Directions to Home")
                .geometry(Point.fromLngLat(1.0, 2.0))
                .placeName("300 Massachusetts Ave NW")
                .id("directions-home")
                .properties(new JsonObject())
                .build();

        work = CarmenFeature.builder().text("Directions to Work")
                .placeName("1509 16th St NW")
                .geometry(Point.fromLngLat(1.0, 2.0))
                .id("directions-work")
                .properties(new JsonObject())
                .build();
    }


    public void getAllDevices() {
        db.collection(Constant.FS_ADDRESS)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            addressModels = new ArrayList<>();
                            for (DocumentSnapshot document : task.getResult()) {

                                Logg.e(TAG, document.getId() + " => " + document.getData());
                                Map<String, Object> hashMap = document.getData();
                                Gson gson = new Gson();
                                JsonElement jsonElement = gson.toJsonTree(hashMap);
                                AddressModel addressModel = gson.fromJson(jsonElement, AddressModel.class);
                                addressModel.setAddress_id(document.getId());
                                addressModels.add(addressModel);
                                // String lat = (String) hashMap.get(Constant.FS_DEVICES_LATITUDE);
                                //  String longi = (String) hashMap.get(Constant.FS_DEVICES_LONGITUDE);
                                /*for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                                    String key = entry.getKey();
                                    Logg.e("autolog", "key: " + key);
                                    String value = (String) entry.getValue();
                                    Logg.e("autolog", "value: " + value);

                                }*/
                                addMarkerOFDevices(addressModel, document.getId());
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }


    private void initView(View headerView) {
        //Navigation header view
        idIvUserProfile = headerView.findViewById(R.id.idIvUserProfile);
        idIvUserName = headerView.findViewById(R.id.idIvUserName);
        idIvUserEmail = headerView.findViewById(R.id.idIvUserEmail);
        setData();
    }

    private void setData() {

        UserDetailModel userDetailModel = Utils.getUserDetail(this);
        if (userDetailModel.getUid() != null) {
            if (userDetailModel.getPhoneNumber() != null) {
                idIvUserEmail.setText(userDetailModel.getPhoneNumber() + "");
            } else if (userDetailModel.getEmail() != null) {
                Picasso.get()
                        .load(userDetailModel.getPhotoPath())
                        .into(idIvUserProfile);
                idIvUserEmail.setText(userDetailModel.getEmail());
            }
            idIvUserName.setText(userDetailModel.getName());
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_booking) {
            Intent intent = new Intent(DashboardActivity.this, CurrentBookingActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_history) {
            Intent intent = new Intent(DashboardActivity.this, BookingHistoryActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_setting) {

        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(DashboardActivity.this, UserAccountActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_logout) {
            Utils.showAlertDialog(DashboardActivity.this, getString(R.string.app_name), getString(R.string.logout_msg), getString(R.string.logout_text), getString(R.string.cancel_text));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(createLocationRequest());
        mLocationSettingsRequest = builder.build();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    private synchronized void buildGoogleApiClient() {
        mMapBoxMap.setOnMarkerClickListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        buildLocationSettingsRequest();
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);

    }

    private LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }


    private void updateCamera() {
        mapView.setCameraDistance(10);
        CameraPosition position = new CameraPosition.Builder()
                .target(mLatLng) // Sets the new camera position
                .zoom(15) // Sets the zoom
                .bearing(180) // Rotate the camera
                .tilt(30) // Set the camera tilt
                .build(); // Creates a CameraPosition from the builder

        mMapBoxMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(position), 7000);
    }

    private void updateMarker(LatLng mLatLng) {
        //mMapBoxMap.removeMarker(UserMarker);

        Logg.e("**update marker", mLatLng + " " + UserMarker.getPosition());

        UserMarker.setPosition(mLatLng);
        mMapBoxMap.updateMarker(UserMarker);
        updateCamera();

        // updateCamera();
        //getAllDevices();
    }

    public void updateBySearch(LatLng mLatLng) {
        UserMarker.setPosition(mLatLng);
        mMapBoxMap.updateMarker(UserMarker);
        updateCamera();
        getAllDevices();
    }


    private void addMarker() {
        mMarker = new MarkerOptions()
                .position(mLatLng)
                .title("Location")
                .snippet("Welcome to you");


        //UserMarker = mMarker.getMarker();

        UserMarker = mMapBoxMap.addMarker(mMarker);
        getAllDevices();
    }

    private void addMarkerOFDevices(AddressModel addressModel, String id) {

        try {
            if (!addressModel.getLng().isEmpty() && !addressModel.getLat().isEmpty() && !addressModel.getState().equals("false")) {
                if (addressModel.getImage() != null && !addressModel.getImage().isEmpty()) {
                    Picasso.get().load(addressModel.getImage()).fetch();
                }
                Logg.e("autolog", "hashMap: " + addressModel);
                String lat = (String) addressModel.getLat();
                String longi = (String) addressModel.getLng();
                LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(longi));
                IconFactory iconFactory = IconFactory.getInstance(DashboardActivity.this);
                com.mapbox.mapboxsdk.annotations.Icon icon = iconFactory.fromBitmap(Utils.drawTextToBitmap(DashboardActivity.this, R.drawable.marker_black, String.valueOf(addressModel.getTotal_space())));

                if (mLatLng != null) {
                    if (Utils.distance(mLatLng.getLatitude(), mLatLng.getLongitude(), latLng.getLatitude(), latLng.getLongitude())) {
                        MarkerOptions mMarker = new MarkerOptions()
                                .position(latLng)
                                .title(addressModel.getAddress_id()).icon(icon)
                                .snippet("Device " + id);
                        Marker marker = mMapBoxMap.addMarker(mMarker);
                        markersOfAddresses.add(marker);
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = createLocationRequest();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (mMarker != null) {
            //updateMarker(mLatLng);
        } else {
            updateCamera();
            addMarker();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:

                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                try {
                    //move to step 6 in onActivityResult to check what action user has taken on settings dialog
                    status.startResolutionForResult(DashboardActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created.");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.i(TAG, "User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i(TAG, "User chose not to make required location settings changes.");
                        break;
                }
                break;
            case REQUEST_CODE_AUTOCOMPLETE:

                try {
                    if (data != null) {
                        CarmenFeature feature = PlaceAutocomplete.getPlace(data);
                        Logg.e(TAG, "searched location " + feature.toJson());
                        Point point = feature.center();
                        LatLng latLng = new LatLng(point.latitude(), point.longitude());
                        mLatLng = latLng;
                        updateBySearch(latLng);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        } else {
            goAndDetectLocation();
        }

    }

    public void goAndDetectLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    this
            ).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    mRequestingLocationUpdates = true;
                    //     setButtonsEnabledState();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        Utils.registerInternetCheckReceiver(this, broadcastReceiver);
        if (mRequestingLocationUpdates) {
            //  Toast.makeText(FusedLocationWithSettingsDialog.this, "location was already on so detecting location now", Toast.LENGTH_SHORT).show();
            startLocationUpdates();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        unregisterReceiver(broadcastReceiver);
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Utils.setsnackbarInReceive(DashboardActivity.this);
        }
    };

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        // Toast.makeText(this, "" + marker.getTitle() + " " + marker.getPosition().getLatitude() + " " + marker.getPosition().getLongitude(), Toast.LENGTH_SHORT).show();

        int zoom = (int) mMapBoxMap.getCameraPosition().zoom;
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().getLatitude(), marker.getPosition().getLongitude()), zoom);
        mMapBoxMap.animateCamera(cu);
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {

            for (int i = 0; i < addressModels.size(); i++) {
                if (addressModels.get(i).getAddress_id().equals(marker.getTitle())) {
                    selectedPosition = i;
                    idTvParkingAddress.setText(addressModels.get(i).getAddress());
                    if (!addressModels.get(i).getImage().isEmpty()) {
                        Picasso.get().load(addressModels.get(i).getImage()).into(idIvParkingImage);
                    }
                    idTvRate.setText(addressModels.get(i).getPrice() + "");
                    idTvTotalSpace.setText(addressModels.get(i).getTotal_space() + "");
                    idTvAvailableSpace.setText(addressModels.get(i).getTotal_space() + "");
                    idTvAway.setText(Utils.distanceInKm(mLatLng.getLatitude(), mLatLng.getLongitude(), marker.getPosition().getLatitude(), marker.getPosition().getLongitude()) + " " + getString(R.string.km_away_text));

                    overView.setVisibility(View.VISIBLE);
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                    searchFab.setVisibility(View.GONE);


                    break;
                }
            }

        }

        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.idTvBook:
                Intent bookingIntent = new Intent(DashboardActivity.this, BookingActivity.class);
                bookingIntent.putExtra("data", new Gson().toJson(addressModels.get(selectedPosition)));
                startActivity(bookingIntent);
                break;
            case R.id.idTvDirection:
                Toast.makeText(this, "Coming soon", Toast.LENGTH_SHORT).show();
                break;

            case R.id.searchFab:
                Intent intent = new PlaceAutocomplete.IntentBuilder()
                        .accessToken(Mapbox.getAccessToken())
                        .placeOptions(PlaceOptions.builder()
                                .backgroundColor(Color.parseColor("#EEEEEE"))
                                // .addInjectedFeature(home)
                                // .addInjectedFeature(work)
                                .limit(10)
                                .build(PlaceOptions.MODE_CARDS))
                        .build(DashboardActivity.this);
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
                break;

            case R.id.idFabMyLocation:

                if (mLatLng != null) {
                    updateMarker(mLatLng);
                }
                break;

            case R.id.overView:
                searchFab.setVisibility(View.VISIBLE);
                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                overView.setVisibility(View.GONE);
                break;
        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    /*public Task<String> getDataFromCloudFunctions(String lat, String lng, int km) {

        Map<String, Object> data = new HashMap<>();
        data.put("lat", "21.216658");
        data.put("lng", "72.844325");
        data.put("km", 3);

        return functions.getHttpsCallable("getLatLng")
                .call().continueWith(new Continuation<HttpsCallableResult, String>() {
                    @Override
                    public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
                        // This continuation runs on either success or failure, but if the task
                        // has failed then getResult() will throw an Exception which will be
                        // propagated down.
                        String result = (String) task.getResult().getData();
                        *//*List<AddressModel> addressModels=new Gson().fromJson(result,new TypeToken<List<AddressModel>>(){}.getType());*//*
                        return result;
                    }
                });
    }*/


}
