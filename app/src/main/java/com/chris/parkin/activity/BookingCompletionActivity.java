package com.chris.parkin.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.receiver.NotificationReceiver;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;

import java.util.Calendar;
import java.util.Date;

public class BookingCompletionActivity extends AppCompatActivity implements View.OnClickListener {

    TextView idTvNotificationHour;
    Button idBtnSet,idBtnSet1,idBtnSet2;
    Intent intent;
    private String fromDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_completion);

        try {
            intent = getIntent();
            //fromDate = "2018-05-21 13:45";
            fromDate = intent.getStringExtra("fromDate");
        } catch (Exception e) {
            e.printStackTrace();
        }


        idTvNotificationHour = findViewById(R.id.idTvNotificationHour);
        idBtnSet = findViewById(R.id.idBtnSet);

        idTvNotificationHour.setOnClickListener(this);
        idBtnSet.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.idTvNotificationHour:
                showDialogOFHour();
                break;
            case R.id.idBtnSet:
                Date d = Utils.getFormattedDate("yyyy-MM-dd HH:mm", "yyyy-MM-dd HH:mm", fromDate);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(d);
                String[] data = idTvNotificationHour.getText().toString().split(" ");
               // calendar.add(Calendar.HOUR, -Integer.parseInt(data[0]));
                calendar.add(Calendar.MINUTE,1);

                    Logg.e("**data", Utils.getDateStringFromTimeStamp(String.valueOf(calendar.getTimeInMillis()), "yyyy-MM-dd HH:mm"));
                    setNotification(calendar.getTimeInMillis(), "you have booked parking at this time "+Utils.getDateStringFromTimeStamp(String.valueOf(calendar.getTimeInMillis()), "yyyy-MM-dd HH:mm") +" don't forget :)");
                break;
        }
    }

    private void setNotification(long timeInMillis, String  ii) {


        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //creating a new intent specifying the broadcast receiver
        Intent i = new Intent(this, NotificationReceiver.class);
        i.putExtra("data", ii + "");

        //creating a pending intent using the intent
        PendingIntent pi = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), i, 0);

        //setting the repeating alarm that will be fired every day
        am.set(AlarmManager.RTC_WAKEUP, timeInMillis, pi);
        Toast.makeText(this, "Alarm is set", Toast.LENGTH_SHORT).show();
        Logg.e("**not", timeInMillis + "");

    }


    public void showDialogOFHour() {
        final CharSequence colors[] = new CharSequence[]{"1 hour", "2 hours", "3 hours", "4 hours", "5 hours", "6 hours"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a hour");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                idTvNotificationHour.setText(colors[which]);
                Toast.makeText(BookingCompletionActivity.this, " selected :" + colors[which], Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
