package com.chris.parkin.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.chris.parkin.R;
import com.chris.parkin.model.UserDetailModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class GetUserDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = GetUserDetailActivity.class.getSimpleName();
    private ViewFlipper mViewFlipper;
    Intent intent;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    Pinview pinview;
    private TextView idTvNext;
    private AppCompatImageView idTvBack;

    // TODO: 11-03-2018 Second Screen (Firstname,Lastname)
    TextView idTvLastName, idTvYourName, idTvVerificationMobile;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_user_detail);

        Bundle extras = getIntent().getExtras();

        // TODO: 11-03-2018 Firebase Instance
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        mViewFlipper = findViewById(R.id.view_flipper);
        pinview = findViewById(R.id.pinview);
        idTvBack = findViewById(R.id.idTvBack);
        idTvNext = findViewById(R.id.idTvNext);
        idTvVerificationMobile = findViewById(R.id.idTvVerificationMobile);

        idTvYourName = findViewById(R.id.idTvYourName);
        idTvLastName = findViewById(R.id.idTvLastName);

        if (extras != null) {
            String userDetail = extras.getString(Constant.INTENT_EXTRA_USERS);
            String lname = extras.getString(Constant.FS_USERS_LASTNAME);
            if (userDetail != null) {
                idTvYourName.setText(userDetail);
                if (!lname.equalsIgnoreCase(""))
                    idTvLastName.setText(lname);
                mViewFlipper.showNext();
                idTvNext.setText(getString(R.string.Save));
            }
        }

        idTvBack.setOnClickListener(this);
        idTvNext.setOnClickListener(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(Constant.OTP_RECEIVER));
        intent = getIntent();
        String phoneNo = intent.getStringExtra(Constant.MOBILE);
        if (phoneNo != null) {
            idTvVerificationMobile.setText(getString(R.string.verification_code_text, intent.getStringExtra(Constant.MOBILE)));
            mobileVerification(phoneNo);
        }
    }

    public void mobileVerification(String phoneNumber) {
        //Utils.showLoading(GetUserDetailActivity.this);
        try {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phoneNumber,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    mCallbacks);
        } catch (Exception e) {
            Utils.showAlertDialog(this, getString(R.string.Error), getString(R.string.SERVICE_NOT_FOUND), getString(R.string.ok_text), "");
        }
    }


    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
            Logg.d(TAG, "onVerificationCompleted:" + credential);

            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            Utils.hideLoading();
            pinview.setEnabled(false);

            Utils.showAlertDialog(GetUserDetailActivity.this, getString(R.string.invalid_data), getString(R.string.valid_mobile_number), getString(R.string.ok_text), "");

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(String verificationId,
                               PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
            Logg.d(TAG, "onCodeSent:" + verificationId);

            // Save verification ID and resending token so we can use them later

            mVerificationId = verificationId;
            //mResendToken = token;


            // ...
        }
    };

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Utils.hideLoading();
                        if (task.isSuccessful()) {

                            Logg.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = task.getResult().getUser();

                            UserDetailModel userDetailModel = new UserDetailModel();
                            userDetailModel.setPhoneNumber(user.getPhoneNumber());
                            userDetailModel.setType(Constant.LOGIN_TYPE_PHONE);
                            userDetailModel.setUid(user.getUid());

                            Utils.setPref(GetUserDetailActivity.this, Constant.USERDETAIL, new Gson().toJson(userDetailModel));

                            mViewFlipper.showNext();
                            idTvNext.setText(getString(R.string.Save));

                            /*Intent ii = new Intent(GetUserDetailActivity.this, DashboardActivity.class);
                            startActivity(ii);
                            finish();*/
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Logg.e(TAG, "signInWithCredential:failure" + task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            Logg.e("receiver", "Got message: " + message);

            pinview.setValue(message);
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, message);
            signInWithPhoneAuthCredential(credential);

        }
    };

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.idTvBack:
                Utils.hideKeyboard(getActivity());
                startActivity(new Intent(getActivity(), LoginActivity.class));
                finish();
                return;
            case R.id.idTvNext:

                Utils.hideKeyboard(getActivity());
                if (idTvNext.getText().toString().equalsIgnoreCase(getString(R.string.Save))) {
                    // TODO: 11-03-2018 Store user detain in firestore database
                    Utils.showLoading(this, getString(R.string.saving_data));
                    fs_saveUserInfo();
                } else {
                    if (pinview.getValue().length() == 6) {
                        try {
                            Utils.showLoading(getActivity(), getString(R.string.OTP_VERIFYING));
                            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, pinview.getValue());
                            signInWithPhoneAuthCredential(credential);
                        } catch (Exception e) {
                            Utils.hideLoading();
                            Utils.showAlertDialog(getActivity(), getString(R.string.Error), getString(R.string.OTP_NOT_SEND), getString(R.string.ok_text), "");
                        }
                    } else
                        Utils.showAlertDialog(getActivity(), getString(R.string.EmptyField), getString(R.string.enter_valid_otp), getString(R.string.ok_text), "");
                }
                return;
        }
    }

    private void fs_saveUserInfo() {
        if (!idTvLastName.getText().toString().trim().equals("") && !idTvYourName.getText().toString().trim().equals("")) {
            Map<String, Object> userDetails = new HashMap<>();
            userDetails.put(Constant.FS_USERS_USERNAME, idTvYourName.getText().toString().trim());
            userDetails.put(Constant.FS_USERS_LASTNAME, idTvLastName.getText().toString().trim());
            userDetails.put(Constant.FS_USERS_IMAGE, Utils.getUserDetail(this).getPhotoPath().isEmpty() ? "" : Utils.getUserDetail(this).getPhotoPath());

            String uid = mAuth.getUid();
            if (uid != null) {
                // Add a new document with a generated ID
                db.collection(Constant.FS_USERS).document(uid).set(userDetails)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "DocumentSnapshot successfully written!");
                                Utils.hideLoading();

                                UserDetailModel userDetailModel = Utils.getUserDetail(GetUserDetailActivity.this);

                                userDetailModel.setName(idTvYourName.getText().toString().trim());
                                userDetailModel.setLname(idTvLastName.getText().toString().trim());
                                Utils.setPref(GetUserDetailActivity.this, Constant.USERDETAIL, new Gson().toJson(userDetailModel));
                                startActivity(new Intent(getActivity(), DashboardActivity.class));
                                finish();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Utils.hideLoading();
                                Utils.showAlertDialog(getActivity(), getString(R.string.FAILURE), e.getMessage(), getString(R.string.ok_text), "");
                            }
                        });
            } else
                Utils.showAlertDialog(getActivity(), getString(R.string.FAILURE), getString(R.string.UserDataNotFound), getString(R.string.ok_text), "");
        } else {
            Toast.makeText(this, R.string.empty_string_text, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideKeyboard(getActivity());
        Utils.registerInternetCheckReceiver(this, broadcastReceiver);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.setsnackbarInReceive(GetUserDetailActivity.this);
        }
    };
    //Methods
}
