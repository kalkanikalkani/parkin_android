package com.chris.parkin.activity;

import android.os.Debug;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.adapter.BookingHistoryAdapter;
import com.chris.parkin.model.AddressModel;
import com.chris.parkin.model.BookingModel;
import com.chris.parkin.model.HistoryModel;
import com.chris.parkin.myinterface.OnLoadMoreListener;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BookingHistoryActivity extends BaseActivity {

    private static final String TAG = BookingHistoryActivity.class.getSimpleName();
    private TextView idTvEmptyView;
    private RecyclerView idRvBookingHistory;
    private BookingHistoryAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<HistoryModel> historyModels;
    protected Handler handler;
    FirebaseFirestore db;
    private DocumentSnapshot lastVisible;
    ProgressBar idProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_history);
        idTvEmptyView = (TextView) findViewById(R.id.idTvEmptyView);
        idRvBookingHistory = (RecyclerView) findViewById(R.id.idRvBookingHistory);
        idProgressBar=findViewById(R.id.idProgressBar);
        historyModels = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        setupToolbar();
        idProgressBar.setVisibility(View.VISIBLE);
        getBookingData();

        handler = new Handler();
        idRvBookingHistory.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        idRvBookingHistory.setLayoutManager(mLayoutManager);
        mAdapter = new BookingHistoryAdapter(historyModels, BookingHistoryActivity.this, idRvBookingHistory);
        idRvBookingHistory.setAdapter(mAdapter);

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //add null , so the adapter will check view_type and show progress bar at bottom
                // historyModels.add(null);
                //  mAdapter.notifyItemInserted(historyModels.size() - 1);

              /*  handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {*/
                //   remove progress item

                //add items one by one
                int start = historyModels.size();
                int end = start + 20;

                       /* for (int i = start + 1; i <= end; i++) {
                            studentList.add(new Student("title " + i, "address " + i));
                            mAdapter.notifyItemInserted(studentList.size());
                        }*/
                getBookingDataAfterLoaded();

                //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                 /*   }
                }, 2000);*/

            }
        });

    }


    // load initial data

    public void setupToolbar() {
        ImageView idIvBack = findViewById(R.id.idIvBack);
        TextView idTvTitle = findViewById(R.id.idTvTitle);
        TextView idTvSubTitle = findViewById(R.id.idTvSubTitle);

        idTvTitle.setText(R.string.booking_history_text);
        idTvSubTitle.setVisibility(View.GONE);

        idIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void getBookingData() {
        try {

            db.collection(Constant.FS_BOOKING).limit(10).whereEqualTo(Constant.FS_BOOKING_USER_ID, Utils.getUserDetail(this).getUid()).whereEqualTo(Constant.FS_BOOKING_STATUS, false).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    idProgressBar.setVisibility(View.GONE);
                    if (task.getResult().size() == 0) {
                        idRvBookingHistory.setVisibility(View.GONE);
                        idTvEmptyView.setVisibility(View.VISIBLE);

                    } else {
                        idRvBookingHistory.setVisibility(View.VISIBLE);
                        idTvEmptyView.setVisibility(View.GONE);
                    }

                    for (DocumentSnapshot document : task.getResult()) {
                        lastVisible = document;

                        Logg.e(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> hashMap = document.getData();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(hashMap);
                        final HistoryModel historyModel = new HistoryModel();
                        BookingModel bookingModel = gson.fromJson(jsonElement, BookingModel.class);
                        bookingModel.setBooking_id(document.getId());
                        historyModel.setBookingModel(bookingModel);
                        db.collection(Constant.FS_ADDRESS).document(bookingModel.getAddress_id()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Map<String, Object> hashMap = documentSnapshot.getData();
                                Gson gson = new Gson();
                                JsonElement jsonElement = gson.toJsonTree(hashMap);
                                AddressModel addressModel = gson.fromJson(jsonElement, AddressModel.class);
                                addressModel.setAddress_id(documentSnapshot.getId());
                                historyModel.setAddressModel(addressModel);
                               /* Toast.makeText(BookingHistoryActivity.this, "" + addressModel.getAddress_id() + " size" + addressModel.getAddress(), Toast.LENGTH_SHORT).show();*/
                                historyModels.add(historyModel);
                                mAdapter.notifyItemInserted(historyModels.size());

                            }

                        });

                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBookingDataAfterLoaded() {
        try {

            db.collection(Constant.FS_BOOKING).startAfter(lastVisible).limit(10).whereEqualTo(Constant.FS_BOOKING_USER_ID, Utils.getUserDetail(this).getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {

                    for (DocumentSnapshot document : task.getResult()) {
                        lastVisible = document;

                        Logg.e(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> hashMap = document.getData();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(hashMap);
                        final HistoryModel historyModel = new HistoryModel();
                        BookingModel bookingModel = gson.fromJson(jsonElement, BookingModel.class);
                        bookingModel.setBooking_id(document.getId());
                        historyModel.setBookingModel(bookingModel);
                        db.collection(Constant.FS_ADDRESS).document(bookingModel.getAddress_id()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Map<String, Object> hashMap = documentSnapshot.getData();
                                Gson gson = new Gson();
                                JsonElement jsonElement = gson.toJsonTree(hashMap);
                                AddressModel addressModel = gson.fromJson(jsonElement, AddressModel.class);
                                addressModel.setAddress_id(documentSnapshot.getId());
                                historyModel.setAddressModel(addressModel);

                                //   historyModels.remove(historyModels.size() - 1);
                                //   mAdapter.notifyItemRemoved(historyModels.size());
                                historyModels.add(historyModel);
                                mAdapter.notifyItemInserted(historyModels.size());
                                mAdapter.setLoaded();
                            }

                        });

                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
