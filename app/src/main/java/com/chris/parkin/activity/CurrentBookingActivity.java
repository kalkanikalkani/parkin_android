package com.chris.parkin.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.adapter.CurrentBookingAdapter;
import com.chris.parkin.model.AddressModel;
import com.chris.parkin.model.BookingModel;
import com.chris.parkin.model.DeviceModel;
import com.chris.parkin.model.HistoryModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Map;

public class CurrentBookingActivity extends AppCompatActivity {

    TextView idTvEmptyView;
    RecyclerView idRvCurrentBooking;
    ArrayList<HistoryModel> historyModels;
    FirebaseFirestore db;
    private LinearLayoutManager mLayoutManager;
    private CurrentBookingAdapter mAdapter;
    private String TAG = CurrentBookingActivity.class.getSimpleName();
    ProgressBar idProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_booking);
        idTvEmptyView = (TextView) findViewById(R.id.idTvEmptyView);
        idRvCurrentBooking = (RecyclerView) findViewById(R.id.idRvCurrentBooking);
        idProgressBar = findViewById(R.id.idProgressBar);
        historyModels = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        setupToolbar();
        idProgressBar.setVisibility(View.VISIBLE);
        getBookingData();

        idRvCurrentBooking.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        idRvCurrentBooking.setLayoutManager(mLayoutManager);
        mAdapter = new CurrentBookingAdapter(historyModels, CurrentBookingActivity.this);
        idRvCurrentBooking.setAdapter(mAdapter);
    }

    public void setupToolbar() {
        ImageView idIvBack = findViewById(R.id.idIvBack);
        TextView idTvTitle = findViewById(R.id.idTvTitle);
        TextView idTvSubTitle = findViewById(R.id.idTvSubTitle);
        idTvTitle.setText(R.string.current_booking_text);
        idTvSubTitle.setVisibility(View.GONE);
        idIvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void getBookingData() {
        try {

            db.collection(Constant.FS_BOOKING).limit(10).whereEqualTo(Constant.FS_BOOKING_USER_ID, Utils.getUserDetail(this).getUid()).whereEqualTo(Constant.FS_BOOKING_STATUS, true).orderBy(Constant.FS_BOOKING_START_TIME, Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    idProgressBar.setVisibility(View.GONE);
                    if (task.getResult().size() == 0) {
                        idRvCurrentBooking.setVisibility(View.GONE);
                        idTvEmptyView.setVisibility(View.VISIBLE);

                    } else {
                        idRvCurrentBooking.setVisibility(View.VISIBLE);
                        idTvEmptyView.setVisibility(View.GONE);
                    }
                    for (DocumentSnapshot document : task.getResult()) {

                        Logg.e(TAG, document.getId() + " => " + document.getData());
                        Map<String, Object> hashMap = document.getData();
                        Gson gson = new Gson();
                        JsonElement jsonElement = gson.toJsonTree(hashMap);
                        final HistoryModel historyModel = new HistoryModel();
                        BookingModel bookingModel = gson.fromJson(jsonElement, BookingModel.class);
                        bookingModel.setBooking_id(document.getId());
                        historyModel.setBookingModel(bookingModel);
                        db.collection(Constant.FS_DEVICES).document(bookingModel.getDevice_id()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Map<String, Object> hashMap = documentSnapshot.getData();
                                Gson gson = new Gson();
                                JsonElement jsonElement = gson.toJsonTree(hashMap);
                                DeviceModel deviceModel = gson.fromJson(jsonElement, DeviceModel.class);
                                deviceModel.setDevice_id(documentSnapshot.getId());
                                historyModel.setDeviceModel(deviceModel);
                            }
                        });
                        db.collection(Constant.FS_ADDRESS).document(bookingModel.getAddress_id()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {
                                Map<String, Object> hashMap = documentSnapshot.getData();
                                Gson gson = new Gson();
                                JsonElement jsonElement = gson.toJsonTree(hashMap);
                                AddressModel addressModel = gson.fromJson(jsonElement, AddressModel.class);
                                addressModel.setAddress_id(documentSnapshot.getId());
                                historyModel.setAddressModel(addressModel);
                              /*  Toast.makeText(CurrentBookingActivity.this, "" + addressModel.getAddress_id() + " size" + addressModel.getAddress(), Toast.LENGTH_SHORT).show();*/
                                historyModels.add(historyModel);
                                mAdapter.notifyItemInserted(historyModels.size());

                            }

                        });

                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
