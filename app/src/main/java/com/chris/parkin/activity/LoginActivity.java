package com.chris.parkin.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chris.parkin.R;
import com.chris.parkin.model.UserDetailModel;
import com.chris.parkin.util.Constant;
import com.chris.parkin.util.Logg;
import com.chris.parkin.util.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.hbb20.CountryCodePicker;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "LoginActivity";
    private FirebaseAuth mAuth;
    private int RC_SIGN_IN = 100;
    GoogleSignInOptions options;
    GoogleApiClient mGoogleApiClient;
    ImageView googleButton, ivSend, facebookButton;
    private CallbackManager mCallbackManager;
    private TextView tvMobileNumber;
    CountryCodePicker countrycodePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);

        setContentView(R.layout.activity_login);

        /*Utils.showDialogOKCancel(this, "ParkIn", "Are you sure you want to sign out?",true,"OK","CANCEL");*/

        mAuth = FirebaseAuth.getInstance();
        getFacebookAuthKey();
        mCallbackManager = CallbackManager.Factory.create();
        options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

        googleButton = (ImageView) findViewById(R.id.ivGoogle);
        facebookButton = (ImageView) findViewById(R.id.ivFacebook);
        ivSend = (ImageView) findViewById(R.id.ivSend);
        tvMobileNumber = (TextView) findViewById(R.id.tvMobileNumber);
        countrycodePicker = findViewById(R.id.countrycodePicker);

        googleButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);
        ivSend.setOnClickListener(this);

    }

    private void signInWithFb() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logg.d(TAG, "facebook:onSuccess:" + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        // Debug.d(TAG, "facebook:onSuccess:" + loginResult);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        exception.printStackTrace();
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                        // Debug.d(TAG, "facebook:onSuccess:" + loginResult);
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void getFacebookAuthKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Logg.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                //WcgthtfIS9zTVFspd053j6uA3mY=
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Logg.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Logg.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            UserDetailModel userDetailModel = new UserDetailModel();
                            String[] fullname = user.getDisplayName().split(" ");
                            userDetailModel.setName(fullname[0]);
                            userDetailModel.setLname(fullname[1]);
                            userDetailModel.setEmail(user.getEmail());
                            userDetailModel.setType(Constant.LOGIN_TYPE_FACEBOOK);
                            userDetailModel.setUid(user.getUid());
                            userDetailModel.setPhotoPath(user.getPhotoUrl().toString());
                            Logg.e("**user detail fb", user.getDisplayName() + " " + user.getEmail());
                            Utils.setUserDetail(LoginActivity.this, userDetailModel);
                            Intent ii = new Intent(LoginActivity.this, GetUserDetailActivity.class);
                            ii.putExtra(Constant.INTENT_EXTRA_USERS, userDetailModel.getName());
                            ii.putExtra(Constant.FS_USERS_LASTNAME, userDetailModel.getLname());
                            startActivity(ii);
                            finish();
                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Logg.e(TAG, "signInWithCredential:failure" + task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            // updateUI(null);
                        }

                        // ...
                    }
                });
    }


    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        Logg.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Utils.hideLoading();
                            Logg.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Logg.e("**user detail", user.getDisplayName() + " " + user.getEmail() + " " + user.getPhotoUrl());

                            UserDetailModel userDetailModel = new UserDetailModel();
                            userDetailModel.setName(acct.getGivenName());
                            userDetailModel.setLname(acct.getFamilyName());
                            userDetailModel.setEmail(user.getEmail());
                            userDetailModel.setType(Constant.LOGIN_TYPE_GOOGLE);
                            userDetailModel.setUid(user.getUid());
                            userDetailModel.setPhotoPath(user.getPhotoUrl().toString());

                            Logg.e("**details", acct.getDisplayName() + "-" + acct.getFamilyName() + "-" + acct.getGivenName());

                            Utils.setUserDetail(LoginActivity.this, userDetailModel);

                            Intent ii = new Intent(LoginActivity.this, GetUserDetailActivity.class);
                            ii.putExtra(Constant.INTENT_EXTRA_USERS, acct.getGivenName());
                            ii.putExtra(Constant.FS_USERS_LASTNAME, acct.getFamilyName());
                            startActivity(ii);
                            finish();

                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Logg.e(TAG, "signInWithCredential:failure" + task.getException());
                            Utils.hideLoading();
                            Utils.showAlertDialog(LoginActivity.this, getString(R.string.wrong_credentials), getString(R.string.authentication_failed), getString(R.string.ok_text), "");
                            // updateUI(null);
                        }

                        // ...
                    }
                });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Logg.e(TAG, "Google sign in failed" + e);
                // ...
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivGoogle:
                if (Utils.haveNetworkConnection(LoginActivity.this)) {
                    Utils.showLoading(LoginActivity.this);
                    signIn();
                } else {
                    Utils.showAlertDialog(LoginActivity.this, getString(R.string.connect_issue), getString(R.string.no_internet_text), getString(R.string.ok_text), "");
                }
                break;
            case R.id.ivFacebook:
                if (Utils.haveNetworkConnection(LoginActivity.this)) {
                    signInWithFb();
                  /*  Utils.showAlertDialog(LoginActivity.this, getString(R.string.facebook_error), getString(R.string.fb_error_text), getString(R.string.ok_text), "");
                    Toast.makeText(LoginActivity.this, R.string.fb_error_text, Toast.LENGTH_SHORT).show();*/
                } else {
                    Utils.showAlertDialog(LoginActivity.this, getString(R.string.connect_issue), getString(R.string.no_internet_text), getString(R.string.ok_text), "");
                }
                break;
            case R.id.ivSend:

                if (Utils.haveNetworkConnection(LoginActivity.this)) {

                    /*startActivity(new Intent(LoginActivity.this,DashboardActivity.class));
                    finish();*/
                    if (tvMobileNumber.getText().length() == getResources().getInteger(R.integer.phone_length)) {
                        Intent intent = new Intent(LoginActivity.this, GetUserDetailActivity.class);
                        intent.putExtra(Constant.MOBILE, countrycodePicker.getSelectedCountryCodeWithPlus() + tvMobileNumber.getText().toString());
                        startActivity(intent);
                        finish();
                    } else
                        Utils.showAlertDialog(LoginActivity.this, getString(R.string.EmptyField), getString(R.string.mobile_required), getString(R.string.ok_text), "");
                } else {
                    Utils.showAlertDialog(LoginActivity.this, getString(R.string.connect_issue), getString(R.string.no_internet_text), getString(R.string.ok_text), "");
                }
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Utils.registerInternetCheckReceiver(this, broadcastReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.setsnackbarInReceive(LoginActivity.this);
        }
    };

}
