package com.chris.parkin.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.chris.parkin.util.Logg;
import com.chris.parkin.util.NotificationHelper;

public class NotificationReceiver extends BroadcastReceiver {

    //the method will be fired when the alarm is triggerred
    @Override
    public void onReceive(Context context, Intent intent) {


        //you can check the log that it is fired
        //Here we are actually not doing anything
        //but you can do any task here that you want to be done at a specific time everyday


     //   Utils.removeNotificationData(context, Integer.parseInt(intent.getExtras().getString("data")));

        Logg.e("**MyAlarmBelal", "Alarm just fired ");
        NotificationHelper notificationHelper=new NotificationHelper(context);
        notificationHelper.createNotification("ParkIn",intent.getExtras().getString("data"));
 //+ intent.getExtras().getString("data")
    }

}

