package com.chris.parkin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chris.parkin.R;
import com.chris.parkin.model.HistoryModel;
import com.chris.parkin.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CurrentBookingAdapter extends RecyclerView.Adapter {


    private Context context;
    private final int VIEW_ITEM = 1;

    private List<HistoryModel> data;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView idTvTitle, idTvAddress, idTvFromDate, idTvFromTime, idTvToDate, idTvToTime, idTvAmount, idTvAllocatedSpace;
        ImageView idIvImage;


        public MyViewHolder(View view) {
            super(view);
            idTvTitle = (TextView) view.findViewById(R.id.idTvTitle);
            idIvImage = (ImageView) view.findViewById(R.id.idIvImage);
            idTvAddress = (TextView) view.findViewById(R.id.idTvAddress);
            idTvFromDate = (TextView) view.findViewById(R.id.idTvFromDate);
            idTvFromTime = (TextView) view.findViewById(R.id.idTvFromTime);
            idTvToDate = (TextView) view.findViewById(R.id.idTvToDate);
            idTvToTime = (TextView) view.findViewById(R.id.idTvToTime);
            idTvAmount = (TextView) view.findViewById(R.id.idTvAmount);
            idTvAllocatedSpace = (TextView) view.findViewById(R.id.idTvAllocatedSpace);


        }
    }


    public CurrentBookingAdapter(List<HistoryModel> students, Context context) {

        this.data = students;
        this.context = context;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_current_booking_layout, parent, false);

        vh = new MyViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {

            HistoryModel data = (HistoryModel) this.data.get(position);

            if (!data.getAddressModel().getImage().isEmpty()) {
                Picasso.get().load(data.getAddressModel().getImage()).into(((MyViewHolder) holder).idIvImage);
            }

            ((MyViewHolder) holder).idTvTitle.setText(data.getAddressModel().getTitle());
            ((MyViewHolder) holder).idTvAddress.setText(data.getAddressModel().getAddress());
            ((MyViewHolder) holder).idTvFromDate.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getStart_time().toString(), "dd MMM, yyyy"));
            ((MyViewHolder) holder).idTvFromTime.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getStart_time().toString(), "hh:mm a"));
            ((MyViewHolder) holder).idTvToDate.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getEnd_time().toString(), "dd MMM, yyyy"));
            ((MyViewHolder) holder).idTvToTime.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getEnd_time().toString(), "hh:mm a"));
            ((MyViewHolder) holder).idTvAmount.setText("$" + data.getBookingModel().getAmount());

            if (data.getDeviceModel().getName() != null) {
                ((MyViewHolder) holder).idTvAllocatedSpace.setText(data.getDeviceModel().getName() + "");
            }
        }
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? VIEW_ITEM : VIEW_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}