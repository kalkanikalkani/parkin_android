package com.chris.parkin.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chris.parkin.R;
import com.chris.parkin.model.HistoryModel;
import com.chris.parkin.myinterface.OnLoadMoreListener;
import com.chris.parkin.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BookingHistoryAdapter extends RecyclerView.Adapter {


    private Context context;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<HistoryModel> data;

    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView idTvTitle, idTvAddress, idTvFromDate, idTvFromTime, idTvToDate, idTvToTime, idTvAmount;
        ImageView idIvImage;


        public MyViewHolder(View view) {
            super(view);
            idTvTitle = (TextView) view.findViewById(R.id.idTvTitle);
            idIvImage=(ImageView)view.findViewById(R.id.idIvImage);
            idTvAddress = (TextView) view.findViewById(R.id.idTvAddress);
            idTvFromDate = (TextView) view.findViewById(R.id.idTvFromDate);
            idTvFromTime = (TextView) view.findViewById(R.id.idTvFromTime);
            idTvToDate = (TextView) view.findViewById(R.id.idTvToDate);
            idTvToTime = (TextView) view.findViewById(R.id.idTvToTime);
            idTvAmount = (TextView) view.findViewById(R.id.idTvAmount);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.idProgressBar);
        }
    }


    public BookingHistoryAdapter(List<HistoryModel> students, Context context, RecyclerView recyclerView) {

        this.data = students;
        this.context = context;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }

    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_booking_history_layout, parent, false);

            vh = new MyViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_bar, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {

            HistoryModel data = (HistoryModel) this.data.get(position);

            if (!data.getAddressModel().getImage().isEmpty()) {
                Picasso.get().load(data.getAddressModel().getImage()).into(((MyViewHolder) holder).idIvImage);
            }

            ((MyViewHolder) holder).idTvTitle.setText(data.getAddressModel().getTitle());
            ((MyViewHolder) holder).idTvAddress.setText(data.getAddressModel().getAddress());

            ((CurrentBookingAdapter.MyViewHolder) holder).idTvFromDate.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getStart_time(), "dd MMM, yyyy"));
            ((CurrentBookingAdapter.MyViewHolder) holder).idTvFromTime.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getStart_time(), "hh:mm a"));
            ((CurrentBookingAdapter.MyViewHolder) holder).idTvToDate.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getEnd_time(), "dd MMM, yyyy"));
            ((CurrentBookingAdapter.MyViewHolder) holder).idTvToTime.setText(Utils.getDateStringFromTimeStamp(data.getBookingModel().getEnd_time(), "hh:mm a"));
            ((MyViewHolder) holder).idTvAmount.setText("$" + data.getBookingModel().getAmount());

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}