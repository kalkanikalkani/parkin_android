package com.chris.parkin.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.chris.parkin.R;
import com.chris.parkin.activity.LoginActivity;
import com.chris.parkin.model.UserDetailModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jayesh on 09-03-2018.
 */

public class Utils {

    private static final String sharedPref = "sharedPref";
    private static ProgressDialog dialog;
    private static Snackbar snackbar;
    private static boolean internetConnected = true;
    private static final String IMAGE_DIRECTORY_NAME = "userImages";
    private static List<String> list;

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static void showLoading(Context context) {
        dialog = ProgressDialog.show(context, "", "Loading. Please wait...", true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void showLoading(Context context, String msg) {
        dialog = ProgressDialog.show(context, "", msg, true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public static void hideLoading() {
        try {
            if (dialog.isShowing() && dialog != null)
                dialog.dismiss();
        } catch (Exception e) {

        }
    }


    public static void setPref(Context context, String key, String val) {
        context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE).edit().putString(key, val).commit();
    }

    public static String getPref(Context context, String key) {
        return context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE).getString(key, "");
    }

    public static void clearAllPref(Context context) {
        context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE).edit().clear().commit();
    }

    public static void setUserDetail(Context context, UserDetailModel userDetailModel) {
        setPref(context, Constant.USERDETAIL, new Gson().toJson(userDetailModel));
    }

    public static UserDetailModel getUserDetail(Context context) {
        UserDetailModel userDetailModel = new Gson().fromJson(getPref(context, Constant.USERDETAIL), UserDetailModel.class);
        return userDetailModel;
    }

    public static void setNotificationArray(Context context, String data) {
        setPref(context, Constant.NOTIFICATION_ARRAY, new Gson().toJson(data));
    }

    public static List<String> getNotificationArray(Context context) {
        list = new ArrayList<>();
        if(!getPref(context,Constant.NOTIFICATION_ARRAY).isEmpty()) {
            list = new Gson().fromJson(getPref(context, Constant.NOTIFICATION_ARRAY), new TypeToken<List<String>>() {
            }.getType());
            if (list == null) {
                list = new ArrayList<>();
            }
        }
        return list;

    }

    public static void removeNotificationData(Context context, int position) {
        List<String> list = getNotificationArray(context);
        list.remove(position);
        setNotificationArray(context, new Gson().toJson(list));
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void registerInternetCheckReceiver(Context context, BroadcastReceiver broadcastReceiver) {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        context.registerReceiver(broadcastReceiver, internetFilter);
    }

    public static void setsnackbarInReceive(Activity activity) {
        boolean status = haveNetworkConnection(activity);
        if (!status) {
            setSnackbarMessage(activity, (CoordinatorLayout) activity.findViewById(R.id.coordinateLayout), activity.getString(R.string.check_connection), false);
        }
        /*else {
            setSnackbarMessage((CoordinatorLayout) activity.findViewById(R.id.coordinateLayout), activity.getString(R.string.internet_connected_text), false);
        }*/
    }


    public static void setSnackbarMessage(final Activity activity, CoordinatorLayout coordinatorLayout, String status, boolean showBar) {

        snackbar = Snackbar
                .make(coordinatorLayout, status, Snackbar.LENGTH_LONG)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        snackbar.dismiss();
                    }
                });
        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);
        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();

    }

    public static void showAlertDialog(final Activity activity, String title, String msg, final String okButtonText, String cancelButtonText) {
       /* final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvMsg = (TextView) dialog.findViewById(R.id.tvMsg);
        final TextView tvOk = (TextView) dialog.findViewById(R.id.tvOk);
        final TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        View view = dialog.findViewById(R.id.view);
        tvTitle.setText(title);
        tvOk.setText(okButtonText);
        tvCancel.setText(cancelButtonText);
        tvMsg.setText(msg);

        if (!cancelButtonText.isEmpty()) {
            tvCancel.setVisibility(View.VISIBLE);
        } else {
            tvCancel.setVisibility(View.GONE);
        }
        if (!okButtonText.isEmpty()) {
            tvOk.setVisibility(View.VISIBLE);
        } else {
            tvOk.setVisibility(View.GONE);
        }
        if (cancelButtonText.isEmpty() || okButtonText.isEmpty()) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
        }

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvOk.getText().toString().equalsIgnoreCase(activity.getString(R.string.logout_text))) {
                    clearAllPref(activity);
                    activity.startActivity(new Intent(activity, LoginActivity.class));
                    activity.finish();
                } else {
                    dialog.dismiss();
                }
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();*/

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);


        if (!cancelButtonText.isEmpty()) {
            alertDialogBuilder.setNegativeButton(cancelButtonText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
        }
        if (!okButtonText.isEmpty()) {
            alertDialogBuilder.setPositiveButton(okButtonText,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (okButtonText.toString().equalsIgnoreCase(activity.getString(R.string.logout_text))) {
                                clearAllPref(activity);
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                                activity.finish();
                            } else {
                                dialogInterface.dismiss();
                            }
                        }
                    });
        }

        Dialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    public static boolean distance(double lat1, double lng1, double lat2, double lng2) {

        Location locationA = new Location("point A");

        locationA.setLatitude(lat1);
        locationA.setLongitude(lng1);

        Location locationB = new Location("point B");

        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);

        float distance = locationA.distanceTo(locationB);


        double dist = (float) (distance) * 0.001;
        Logg.e("**distance", dist + "");

        if (dist <= 3) {
            return true;
        } else {
            return true;
        }
    }

    public static int distanceInKm(double lat1, double lng1, double lat2, double lng2) {

        Location locationA = new Location("point A");

        locationA.setLatitude(lat1);
        locationA.setLongitude(lng1);

        Location locationB = new Location("point B");

        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);

        float distance = locationA.distanceTo(locationB);


        double dist = (float) (distance) * 0.001;
        Logg.e("**distance", dist + "");


        return (int) dist;


    }

    public static Bitmap drawTextToBitmap(Context mContext, int resourceId, String mText) {
        Bitmap newBitmap = null;

        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            Bitmap bm1 = BitmapFactory.decodeResource(resources, resourceId);

            Bitmap.Config config = bm1.getConfig();
            if (config == null) {
                config = Bitmap.Config.ARGB_8888;
            }

            newBitmap = Bitmap.createBitmap(bm1.getWidth(), bm1.getHeight(), config);
            Canvas newCanvas = new Canvas(newBitmap);

            newCanvas.drawBitmap(bm1, 0, 0, null);


            if (mText != null) {

                int MY_DIP_VALUE = 15;
                Paint paintText = new Paint(Paint.ANTI_ALIAS_FLAG);
                paintText.setColor(Color.BLACK);

                int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                        MY_DIP_VALUE, mContext.getResources().getDisplayMetrics());

                paintText.setTextSize(pixel);
                paintText.setStyle(Paint.Style.FILL);
                // paintText.setShadowLayer(10f, 10f, 10f, Color.BLACK);

                Rect rectText = new Rect();

                paintText.getTextBounds(mText, 0, mText.length(), rectText);

                int xPos = (newCanvas.getWidth() / 2) - 20;
                int yPos = (int) ((newCanvas.getHeight() / 2));
                newCanvas.drawText(mText,
                        xPos, yPos, paintText);


            } else {

            }

            return newBitmap;

        } catch (Exception e) {

        }
        return newBitmap;
    }

    public static Date getFormattedDate(String defaultFormat, String mainFormat, String dateString) {
        Date date = null;
        SimpleDateFormat defaultFromatter = new SimpleDateFormat(defaultFormat);
        SimpleDateFormat mainFormatter = new SimpleDateFormat(mainFormat);

        try {
            date = mainFormatter.parse(mainFormatter.format(defaultFromatter.parse(dateString)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getTimeStampFromString(String defaultFormat, String dateString) {
        Date date = null;
        String timestamp = null;
        SimpleDateFormat defaultFromatter = new SimpleDateFormat(defaultFormat);

        try {
            date = defaultFromatter.parse(dateString);
            timestamp = String.valueOf(date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timestamp;
    }

    public static String getTimeStampFromCurrentDayEndDateTime(String date) {
        String s = null;
        Calendar calendar = Calendar.getInstance();
        String[] a = date.split("-");
        calendar.set(Integer.parseInt(a[0]), Integer.parseInt(a[1]), Integer.parseInt(a[2]), 24, 00);
        s = String.valueOf(calendar.getTimeInMillis());

        return s;
    }


    public static String getDateStringFromTimeStamp(String timeStamp, String mainFormat) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Long.parseLong(timeStamp));
        //String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        SimpleDateFormat mainFormatter = new SimpleDateFormat(mainFormat);
        String dateString = mainFormatter.format(cal.getTime());
        return dateString;
    }


    public static String getFormattedDateString(String defaultFormat, String mainFormat, String dateString) {
        String date = null;
        SimpleDateFormat defaultFromatter = new SimpleDateFormat(defaultFormat);
        SimpleDateFormat mainFormatter = new SimpleDateFormat(mainFormat);

        try {
            date = mainFormatter.format(defaultFromatter.parse(dateString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String twoDigitString(int s) {
        return String.format("%02d", s);
    }

    public static long getMinutesFromDate(Date End_date, Date Start_date) {
        long seconds, minutes;
        long diffInMillisec = End_date.getTime() - Start_date.getTime();
        seconds = diffInMillisec / 1000;
        minutes = seconds / 60;
        Logg.e("**getMinutesFromDate", diffInMillisec + " " + " " + seconds + " " + minutes);
        return minutes;
    }

    public static File getOutputMediaFile() {
        try {
// To be safe, you should check that the SDCard is mounted
// using Environment.getExternalStorageState() before doing this.
            File mediaStorageDir;
            if (isSDcardMounted()) {
                mediaStorageDir = new File(
                        android.os.Environment.getExternalStorageDirectory(),
                        IMAGE_DIRECTORY_NAME);
            } else {
                mediaStorageDir = new File(Environment.getRootDirectory(),
                        IMAGE_DIRECTORY_NAME);
            }

// Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
// Create a media file name
            File mediaFile = new File(mediaStorageDir.getPath()
                    + File.separator + new Date().getTime() + ".jpg");
// mediaFile.createNewFile();

            return mediaFile;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isSDcardMounted() {

        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            return true;
        }

        return false;
    }

    public static boolean checkPermissionStorage(Context context) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;

    }


}
