package com.chris.parkin.util;

import android.util.Log;

/**
 * Created by Jayesh on 10-03-2018.
 */

public class Logg {

    private static final boolean DEBUG = true;


    public static void e(String key, String val) {
        if (DEBUG) {
            android.util.Log.e(key, val);
        }
    }

    public static void i(String key, String val) {
        if (DEBUG) {
            android.util.Log.i(key, val);
        }
    }

    public static void d(String key, String val) {
        if (DEBUG) {
            android.util.Log.d(key, val);
        }
    }

    public static void v(String key, String val) {
        if (DEBUG) {
            android.util.Log.v(key, val);
        }
    }


}
