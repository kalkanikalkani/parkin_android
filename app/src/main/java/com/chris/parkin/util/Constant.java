package com.chris.parkin.util;

/**
 * Created by Jayesh on 09-03-2018.
 */

public class Constant {

    // TODO: 12-03-2018 Intent Put Extra
    public static final String INTENT_EXTRA_USERS = "USERS_INFO";

    public static final String USERDETAIL = "userDetail";
    public static final String NOTIFICATION_ARRAY = "NotificationData";

    public static final String IS_LOGIN = "isLogin";

    public static final String LOGIN_TYPE_PHONE = "phone";
    public static final String LOGIN_TYPE_GOOGLE = "google";
    public static final String LOGIN_TYPE_FACEBOOK = "facebook";
    public static final String OTP_RECEIVER = "msgReceiver";

    // TODO: 11-03-2018 Firestore Unique Keyword

    //devices table
    public static final String FS_DEVICES = "devices";
    public static final String FS_DEVICES_ADDRESS_ID = "address_id";
    public static final String FS_DEVICES_IP_ADDRESS = "ip_address";
    public static final String FS_DEVICES_IS_BOOKING = "is_booking";
    public static final String FS_DEVICES_NAME = "name";
    public static final String FS_DEVICES_STATUS = "status";

    //address table
    public static final String FS_ADDRESS = "address";
    public static final String FS_ADDRESS_ADDRESS = "address";
    public static final String FS_ADDRESS_CITY = "city";
    public static final String FS_ADDRESS_COUNTRY = "country";
    public static final String FS_ADDRESS_IMAGE = "image";
    public static final String FS_ADDRESS_LAT = "lat";
    public static final String FS_ADDRESS_LNG = "lng";
    public static final String FS_ADDRESS_PONCODE = "pin_code";
    public static final String FS_ADDRESS_PRICE = "price";
    public static final String FS_ADDRESS_STATE = "state";
    public static final String FS_ADDRESS_STATUS = "status";
    public static final String FS_ADDRESS_TITLE = "title";
    public static final String FS_ADDRESS_TOTALFLOOR = "total_floor";
    public static final String FS_ADDRESS_TOTALSPACE = "total_space";

    //booking table
    public static final String FS_BOOKING = "booking";
    public static final String FS_BOOKING_ADDRESS_ID = "address_id";
    public static final String FS_BOOKING_AMOUNT = "amount";
    public static final String FS_BOOKING_BOOKING_ID = "booking_id";
    public static final String FS_BOOKING_DEVICE_ID = "device_id";
    public static final String FS_BOOKING_END_TIME = "end_time";
    public static final String FS_BOOKING_START_TIME = "start_time";
    public static final String FS_BOOKING_STATUS = "status";
    public static final String FS_BOOKING_USER_ID = "user_id";


    //payment table
    public static final String FS_PAYMENT = "payment";
    public static final String FS_PAYMENT_AMOUNT = "amount";
    public static final String FS_PAYMENT_BOOKING_ID = "booking_id";
    public static final String FS_PAYMENT_CREATED_TIME = "created_time";
    public static final String FS_PAYMENT_PAYMENT_ID = "payment_id";
    public static final String FS_PAYMENT_PLATFORM = "platform";
    public static final String FS_PAYMENT_STATUS = "status";
    public static final String FS_PAYMENT_USER_ID = "user_id";


    //users table
    public static final String FS_USERS = "users";
    public static final String FS_USERS_USERNAME = "firstname";
    public static final String FS_USERS_LASTNAME = "lastname";
    public static final String FS_USERS_IMAGE = "image";

    public static final String MOBILE = "mNumber";

}