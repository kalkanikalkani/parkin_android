package com.chris.parkin.myinterface;

/**
 * Created by Jayesh on 18-04-2018.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
