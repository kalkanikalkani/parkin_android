package com.chris.parkin.model;

/**
 * Created by Jayesh on 09-03-2018.
 */

public class UserDetailModel {
    String uid;
    String lname;
    String name;
    String email;
    String phoneNumber;
    String type;
    String photoPath;

    /*public UserDetailModel(String name, String email, String type,String uid) {
        this.uid = uid;
        this.name = name;
        this.email = email;
        this.type=type;
    }

    public UserDetailModel(String phoneNumber, String type,String uid,String photoPath) {
        this.uid = uid;
        this.phoneNumber = phoneNumber;
        this.type=type;
        this.photoPath=photoPath;
    }*/

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
