package com.chris.parkin.model;

import com.chris.parkin.util.Constant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Jayesh on 25-04-2018.
 */

public class PaymentModel {
    @SerializedName(Constant.FS_PAYMENT_AMOUNT)
    @Expose
    String amount;
    @SerializedName(Constant.FS_PAYMENT_BOOKING_ID)
    @Expose
    String booking_id;
    @SerializedName(Constant.FS_PAYMENT_CREATED_TIME)
    @Expose
    Date created_time;
    @SerializedName(Constant.FS_PAYMENT_PAYMENT_ID)
    @Expose
    String payment_id;
    @SerializedName(Constant.FS_PAYMENT_PLATFORM)
    @Expose
    String platform;
    @SerializedName(Constant.FS_PAYMENT_STATUS)
    @Expose
    String status;
    @SerializedName(Constant.FS_PAYMENT_USER_ID)
    @Expose
    String user_id;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public Date getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Date created_time) {
        this.created_time = created_time;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
