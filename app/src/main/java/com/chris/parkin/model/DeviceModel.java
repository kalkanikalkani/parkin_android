package com.chris.parkin.model;

import com.chris.parkin.util.Constant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jayesh on 23-04-2018.
 */

public class DeviceModel {

    String device_id;
    @SerializedName(Constant.FS_DEVICES_ADDRESS_ID)
    @Expose
    String address_id;
    @SerializedName(Constant.FS_DEVICES_IP_ADDRESS)
    @Expose
    String ip_address;
    @SerializedName(Constant.FS_DEVICES_IS_BOOKING)
    @Expose
    boolean is_booking;
    @SerializedName(Constant.FS_DEVICES_NAME)
    @Expose
    String name;
    @SerializedName(Constant.FS_DEVICES_STATUS)
    @Expose
    boolean status;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public boolean isIs_booking() {
        return is_booking;
    }

    public void setIs_booking(boolean is_booking) {
        this.is_booking = is_booking;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
