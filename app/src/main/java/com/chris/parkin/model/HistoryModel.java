package com.chris.parkin.model;

/**
 * Created by Jayesh on 19-04-2018.
 */

public class HistoryModel {

    private AddressModel addressModel;
    private BookingModel bookingModel;
    private DeviceModel deviceModel;


    public DeviceModel getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(DeviceModel deviceModel) {
        this.deviceModel = deviceModel;
    }

    public AddressModel getAddressModel() {
        return addressModel;
    }

    public void setAddressModel(AddressModel addressModel) {
        this.addressModel = addressModel;
    }

    public BookingModel getBookingModel() {
        return bookingModel;
    }

    public void setBookingModel(BookingModel bookingModel) {
        this.bookingModel = bookingModel;
    }
}
