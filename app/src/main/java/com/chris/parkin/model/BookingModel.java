package com.chris.parkin.model;

import com.chris.parkin.util.Constant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Jayesh on 19-04-2018.
 */

public class BookingModel {
    @SerializedName(Constant.FS_BOOKING_BOOKING_ID)
    @Expose
    String booking_id;
    @SerializedName(Constant.FS_BOOKING_AMOUNT)
    @Expose
    String amount;
    @SerializedName(Constant.FS_BOOKING_DEVICE_ID)
    @Expose
    String device_id;
    @SerializedName(Constant.FS_BOOKING_END_TIME)
    @Expose
    String end_time;
    @SerializedName(Constant.FS_BOOKING_START_TIME)
    @Expose
    String start_time;
    @SerializedName(Constant.FS_BOOKING_USER_ID)
    @Expose
    String user_id;
    @SerializedName(Constant.FS_BOOKING_ADDRESS_ID)
    @Expose
    String address_id;
    @SerializedName(Constant.FS_BOOKING_STATUS)
    @Expose
    boolean status;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
