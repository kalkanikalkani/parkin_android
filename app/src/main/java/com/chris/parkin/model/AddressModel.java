package com.chris.parkin.model;

import com.chris.parkin.util.Constant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jayesh on 22-03-2018.
 */

public class AddressModel {


    String address_id;

    @SerializedName(Constant.FS_ADDRESS_ADDRESS)
    @Expose
    String address;
    @SerializedName(Constant.FS_ADDRESS_TITLE)
    @Expose
    String title;
    @SerializedName(Constant.FS_ADDRESS_STATUS)
    @Expose
    boolean status;
    @SerializedName(Constant.FS_ADDRESS_IMAGE)
    @Expose
    String image;
    @SerializedName(Constant.FS_ADDRESS_CITY)
    @Expose
    String city;
    @SerializedName(Constant.FS_ADDRESS_COUNTRY)
    @Expose
    String country;
    @SerializedName(Constant.FS_ADDRESS_STATE)
    @Expose
    String state;
    @SerializedName(Constant.FS_ADDRESS_TOTALFLOOR)
    @Expose
    int total_floor;
    @SerializedName(Constant.FS_ADDRESS_TOTALSPACE)
    @Expose
    int total_space;
    @SerializedName(Constant.FS_ADDRESS_LAT)
    @Expose
    String lat;
    @SerializedName(Constant.FS_ADDRESS_LNG)
    @Expose
    String lng;
    @SerializedName(Constant.FS_ADDRESS_PONCODE)
    @Expose
    int pin_code;
    @SerializedName(Constant.FS_ADDRESS_PRICE)
    @Expose
    int price;

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getTotal_floor() {
        return total_floor;
    }

    public void setTotal_floor(int total_floor) {
        this.total_floor = total_floor;
    }

    public int getTotal_space() {
        return total_space;
    }

    public void setTotal_space(int total_space) {
        this.total_space = total_space;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getPin_code() {
        return pin_code;
    }

    public void setPin_code(int pin_code) {
        this.pin_code = pin_code;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
