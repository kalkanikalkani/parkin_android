package com.chris.parkin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jayesh on 25-04-2018.
 */

public class PaypalPaymentModel {



        @SerializedName("client")
        @Expose
        private Client client;
        @SerializedName("response")
        @Expose
        private Response response;
        @SerializedName("response_type")
        @Expose
        private String responseType;

        public Client getClient() {
            return client;
        }

        public void setClient(Client client) {
            this.client = client;
        }

        public Response getResponse() {
            return response;
        }

        public void setResponse(Response response) {
            this.response = response;
        }

        public String getResponseType() {
            return responseType;
        }

        public void setResponseType(String responseType) {
            this.responseType = responseType;
        }

    public class Response {

        @SerializedName("create_time")
        @Expose
        private String createTime;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("intent")
        @Expose
        private String intent;
        @SerializedName("state")
        @Expose
        private String state;

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIntent() {
            return intent;
        }

        public void setIntent(String intent) {
            this.intent = intent;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }
    public class Client {

        @SerializedName("environment")
        @Expose
        private String environment;
        @SerializedName("paypal_sdk_version")
        @Expose
        private String paypalSdkVersion;
        @SerializedName("platform")
        @Expose
        private String platform;
        @SerializedName("product_name")
        @Expose
        private String productName;

        public String getEnvironment() {
            return environment;
        }

        public void setEnvironment(String environment) {
            this.environment = environment;
        }

        public String getPaypalSdkVersion() {
            return paypalSdkVersion;
        }

        public void setPaypalSdkVersion(String paypalSdkVersion) {
            this.paypalSdkVersion = paypalSdkVersion;
        }

        public String getPlatform() {
            return platform;
        }

        public void setPlatform(String platform) {
            this.platform = platform;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

    }
}
